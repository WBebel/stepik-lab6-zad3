import { useState, useEffect } from 'react'

function App() {

  const [todos, setTodos] = useState([])

  useEffect(() => {
    fetch('http://localhost:5000/todos')
      .then(res => res.json())
      .then(todos => setTodos(todos))
  }, [])

  const addTodo = (text) => {
    fetch('http://localhost:5000/todos', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ text })
  })
    .then(res => res.json())
    .then(todo => setTodos([...todos, todo]))
  }

  return (
    <div className='app'>
      <h1>Todo list</h1>

      <form onSubmit={e => {
        e.preventDefault()
        addTodo(e.target.elements.todo.value)
        e.target.reset()
      }}>
        <input name="todo" />
        <button type="submit">Add</button>
      </form>

      {todos.map(todo => <div key={todo._id}>{todo.text}</div>)}
    </div>
  )
}

export default App
