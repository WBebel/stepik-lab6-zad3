const mongoose = require('mongoose')
const express = require('express')
const cors = require('cors')
const app = express()
app.use(express.json())
app.use(cors())

const port = 5000

const Schema = mongoose.Schema

const todoSchema = new Schema({
    text: String,
})

const Todo = mongoose.model('Todo', todoSchema)

mongoose.connect('mongodb://db:27017')

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/todos', async (req, res) => {
    const todos = await Todo.find()
    res.json(todos)
})

app.post('/todos', async (req, res) => {
    const todo = new Todo({
        text: req.body.text,
    })
    await todo.save()
    res.json(todo)
})

app.listen(port)